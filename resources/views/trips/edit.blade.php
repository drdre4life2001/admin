@extends('layouts.master')

@section('content')

    <div class="content-header">
        <h2 class="content-header-title">Edit Trip </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('trips') }}">Trips </a></li>
          <li class="active">Edit Trip </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">

                {!! Form::model($trip, [
                    'method' => 'PATCH',
                    'url' => ['trips', $trip->id],
                    'class' => 'form-horizontal'
                ]) !!}

                            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                {!! $errors->first('name', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('duration') ? 'has-error' : ''}}">
                {!! Form::label('duration', 'Duration: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('duration', null, ['class' => 'form-control']) !!}
                {!! $errors->first('duration', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('fare') ? 'has-error' : ''}}">
                {!! Form::label('fare', 'Fare: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('fare', null, ['class' => 'form-control']) !!}
                {!! $errors->first('fare', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('ac') ? 'has-error' : ''}}">
                {!! Form::label('ac', 'Ac: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('ac', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('ac', '0', true) !!} No</label>
            </div>
                {!! $errors->first('ac', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('tv') ? 'has-error' : ''}}">
                {!! Form::label('tv', 'Tv: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('tv', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('tv', '0', true) !!} No</label>
            </div>
                {!! $errors->first('tv', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('insurance') ? 'has-error' : ''}}">
                {!! Form::label('insurance', 'Insurance: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('insurance', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('insurance', '0', true) !!} No</label>
            </div>
                {!! $errors->first('insurance', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('passport') ? 'has-error' : ''}}">
                {!! Form::label('passport', 'Passport: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('passport', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('passport', '0', true) !!} No</label>
            </div>
                {!! $errors->first('passport', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-3">
                        {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
                    </div>
                </div>
                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['trips', $trip->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<i class="fa fa-times"></i> Delete Trip', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                {!! Form::close() !!}
            </li>
            <li>
              <a href="{{ url('trips') }}">
                <i class="fa fa-bars"></i> 
                List Trips
              </a>
            </li>
            <li class="">
              <a href="{{ url('trips/create') }}">
                <i class="fa fa-plus"></i> 
                Add Trip
              </a>
            </li>
          </ul>

        </div>

      </div> <!-- /.row -->   

@endsection