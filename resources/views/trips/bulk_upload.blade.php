@extends('layouts.master')

@section('content')

    <div class="content-header">
        <h2 class="content-header-title">Trips </h2>
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}">Dashboard</a></li>
            <li class="active">Trips </li>
        </ol>
    </div> <!-- /.content-header -->




    <div class="row">

        <div class="col-md-10 col-sm-8">

            <div class="portlet">

                <div class="portlet-content">

                    @if(!empty($flash))
                        @foreach($flash as $f)
                            <div class="alert alert-info">
                                <?php echo $f; ?></div>
                        @endforeach
                    @endif

                    <h3>Bulk update old Trips</h3>
                        <br/>
                        <br/>

                        <form action="{{ route('update-trip') }}" method="post" type='file' enctype="multipart/form-data">
                    {{--{!! Form::open(array('route'=>'update-trip','method'=>'POST', 'files'=>true)) !!}--}}
                    {!! csrf_field() !!}
                {{--<input type="file" name="userfile" required>--}}
                   {!! Form::file('userfile') !!}
                    <br/>
                    <br/>
                    {{--<select name="type">--}}
                        {{--<option>Select Upload Type</option>--}}
                        {{--<option value="0">New Upload</option>--}}
                        {{--<option value="1">Update Trips</option>--}}
                    {{--</select>--}}
                     <div class="row" style="width: 20%;">
                            <select  class="form-control" name="operator_id" id="operator_id" class=" " required>
                                <option value="{{ old('username') }}">Choose Operator </option>
                                @foreach($operators as $operator)
                                    <option value="{{  $operator->id }}" >{{  $operator->name }}</option>--}}
                                @endforeach
                            </select>
                              <br>
                          </div>

                    <input type="submit" value="Update Trips" class="btn btn-danger">
                    </form>
                    <form action="{{ route('upload-trip') }}" method="post" type='file' enctype="multipart/form-data">
                        {{--{!! Form::open(array('route'=>'upload-trip','method'=>'POST', 'files'=>true)) !!}--}}

                        <br/>
                        <h3>Bulk upload new Trips</h3>
                        <br/>
                        {!! csrf_field() !!}
                     <input type="file" name="userfile" required>
                        {{--{!! Form::file('userfile') !!}--}}
                        <br/>
                        <br/>
                          <div class="row" style="width: 20%;">
                            <select  class="form-control" name="operator_id" id="operator_id" class=" " required>
                                <option value="{{ old('username') }}">Choose Operator </option>
                                @foreach($operators as $operator)
                                    <option value="{{  $operator->id }}" >{{  $operator->name }}</option>--}}
                                @endforeach
                            </select>
                              <br>
                          </div>

                        <input type="submit" value="Upload new Trips" class="btn btn-danger">
                        </form>

                </div>
            </div>
        </div>
    </div>


@endsection
