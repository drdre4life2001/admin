@extends('layouts.master')

@section('content')


<div class="content-header">
    <h2 class="content-header-title">Add Trip </h2>
    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Dashboard</a></li>
        <li><a href="{{ url('trips') }}">Trips </a></li>
        <li class="active">Add Trip</li>
    </ol>
</div> <!-- /.content-header -->


<div class="row">

<div class="col-md-10 col-sm-8">

    <div class="portlet">

        <div class="portlet-content">

            {!! Form::open(['url' => 'trips', 'class' => 'form-horizontal']) !!}
<!--            <input type="hidden" name="operator_id" value="1">
-->            
			<div class="form-group">
                {!! Form::label('parent_trip_id', 'Parent Trip: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <select name="parent_trip_id" class="form-control select22">
                        <option value="">Choose Parent Trip (If Applicable)</option>
                        @foreach($parent_trips as $trip)
                        <option value="{{ $trip->id }}">{{ $trip->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
			<div class="form-group {{ $errors->has('source_park') ? 'has-error' : ''}}">
                {!! Form::label('source_park', 'Source Park: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <select name="source_park" class="form-control select22">
                        <option value="">Choose Park</option>
                        @foreach($boardable_parks as $park)
                        <option value="{{ $park->id }}">{{ $park->name }}</option>
                        @endforeach
                    </select>
                    {!! $errors->first('source_park', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


            <div class="form-group {{ $errors->has('dest_park') ? 'has-error' : ''}}">
                {!! Form::label('dest_park', 'Destination Park: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <select name="dest_park" class="form-control select22">
                        <option value="">Choose Park</option>
                        @foreach($all_parks as $park)
                        <option value="{{ $park->id }}">{{ $park->name }}</option>
                        @endforeach
                    </select>
                    {!! $errors->first('dest_park', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


            <div class="form-group {{ $errors->has('duration') ? 'has-error' : ''}}">
                {!! Form::label('duration', 'Duration: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('duration', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('duration', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('fare') ? 'has-error' : ''}}">
                {!! Form::label('fare', 'Fare: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('fare', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('fare', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


            <div class="form-group {{ $errors->has('no_of_seats') ? 'has-error' : ''}}">
                {!! Form::label('no_of_seats', 'No Of Seats: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('no_of_seats', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('no_of_seats', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('departure_time') ? 'has-error' : ''}}">
                {!! Form::label('departure_time', 'Departure Time: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::input('time', 'departure_time', null, ['class' => 'form-control', 'id'=>'timepicker1'])
                    !!}
                    <!-- <input id="time-2" name="time-2" class="form-control ui-timepicker parsley-validated parsley-success" data-value="" type="text" data-required="true"> -->
                    {!! $errors->first('departure_time', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('bus_type') ? 'has-error' : ''}}">
                {!! Form::label('bus_type', 'Bus Type: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <select name="bus_type" class="form-control">
                        <option value="">Choose Bus</option>
                        @foreach($bus_types as $bus)
                        <option value="{{ $bus->id }}">{{ $bus->name }}</option>
                        @endforeach
                    </select>
                    {!! $errors->first('bus_type', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('ac') ? 'has-error' : ''}}">
                {!! Form::label('international', 'International?: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <div class="checkbox">
                        <label>{!! Form::radio('international', '1') !!} Yes</label>
                    </div>
                    <div class="checkbox">
                        <label>{!! Form::radio('international', '0', true) !!} No</label>
                    </div>
                    {!! $errors->first('international', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('ac') ? 'has-error' : ''}}">
                {!! Form::label('ac', 'Ac: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <div class="checkbox">
                        <label>{!! Form::radio('ac', '1') !!} Yes</label>
                    </div>
                    <div class="checkbox">
                        <label>{!! Form::radio('ac', '0', true) !!} No</label>
                    </div>
                    {!! $errors->first('ac', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('tv') ? 'has-error' : ''}}">
                {!! Form::label('tv', 'Tv: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <div class="checkbox">
                        <label>{!! Form::radio('tv', '1') !!} Yes</label>
                    </div>
                    <div class="checkbox">
                        <label>{!! Form::radio('tv', '0', true) !!} No</label>
                    </div>
                    {!! $errors->first('tv', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


            <div class="form-group {{ $errors->has('insurance') ? 'has-error' : ''}}">
                {!! Form::label('insurance', 'Insurance: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <div class="checkbox">
                        <label>{!! Form::radio('insurance', '1') !!} Yes</label>
                    </div>
                    <div class="checkbox">
                        <label>{!! Form::radio('insurance', '0', true) !!} No</label>
                    </div>
                    {!! $errors->first('insurance', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <!--<div class="form-group {{ $errors->has('passport') ? 'has-error' : ''}}">
                {!! Form::label('passport', 'Passport: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <div class="checkbox">
                        <label>{!! Form::radio('passport', '1') !!} Yes</label>
                    </div>
                    <div class="checkbox">
                        <label>{!! Form::radio('passport', '0', true) !!} No</label>
                    </div>
                    {!! $errors->first('passport', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>-->
            <div class="form-group {{ $errors->has('virgin_passport') ? 'has-error' : ''}}">
                {!! Form::label('virgin_passport', 'Cost of Virgin Passports: ',
                ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('virgin_passport', 0, ['class' => 'form-control'],['value' => 0]) !!}
                    {!! $errors->first('virgin_passport', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('regular_passport') ? 'has-error' : ''}}">
                {!! Form::label('regular_passport', 'Cost of Regular Passports: ',
                ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('regular_passport', 0, ['class' => 'form-control'],['value' => 0]) !!}
                    {!! $errors->first('regular_passport', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('regular_passport') ? 'has-error' : ''}}">
                {!! Form::label('no_passport', 'Cost of No Passports: ',
                ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('no_passport', 0, ['class' => 'form-control'],['value' => 0]) !!}
                    {!! $errors->first('no_passport', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('tv') ? 'has-error' : ''}}">
                {!! Form::label('tv', 'Tv: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <div class="checkbox">
                        <label>{!! Form::radio('tv', '1') !!} Yes</label>
                    </div>
                    <div class="checkbox">
                        <label>{!! Form::radio('tv', '0', true) !!} No</label>
                    </div>
                    {!! $errors->first('tv', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


            <div class="form-group {{ $errors->has('active') ? 'has-error' : ''}}">
                {!! Form::label('active', 'Active: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <div class="checkbox">
                        <label>{!! Form::radio('active', '1') !!} Yes</label>
                    </div>
                    <div class="checkbox">
                        <label>{!! Form::radio('active', '0', true) !!} No</label>
                    </div>
                    {!! $errors->first('active', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-3">
                    {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
                </div>
            </div>
            </form>

            @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            @endif
        </div>
        <!-- /.portlet-content -->

    </div>
    <!-- /.portlet -->


</div>
<!-- /.col -->

<div class="col-md-2 col-sm-4">

    <ul id="myTab" class="nav nav-pills nav-stacked">
        <li class="active">
            <a href="{{ url('trips') }}">
                <i class="fa fa-bars"></i>
                List Trips
            </a>
        </li>
    </ul>

</div>

</div> <!-- /.row -->


@endsection