@extends('layouts.master')

@section('content')

    <h1>Userlog</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>User Id</th><th>Log Type</th><th>Details</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $userlog->id }}</td> <td> {{ $userlog->user_id }} </td><td> {{ $userlog->log_type }} </td><td> {{ $userlog->details }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection