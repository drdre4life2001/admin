@extends('layouts.master')

@section('content')

   <div class="content-header">
        <h2 class="content-header-title">Add Booking </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('bookings') }}">Bookings </a></li>
          <li class="active">Add Booking </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-9 col-sm-7">

          <div class="portlet">

            <div class="portlet-content">

            @if(Session::has('error'))
                <div class="alert alert-danger">
                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                    <strong>Oh snap!</strong>  {!! session('error') !!}
                  </div>

            @endif

                {!! Form::open(['class' => 'form-horizontal']) !!}

           
            <div class="form-group {{ $errors->has('trip_name') ? 'has-error' : ''}}">
                {!! Form::label('trip_name', 'Trip Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                <input type="text" name="trip" value="{{ $trip_name }}" class="form-control" disabled>
                {!! $errors->first('trip_name', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('booking_date') ? 'has-error' : ''}}">
                {!! Form::label('booking_date', 'Booking Date: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                <input type="text" name="trip" value="{{ $date }}" class="form-control" disabled>
                {!! $errors->first('booking_date', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            
            <div class="form-group {{ $errors->has('passenger_count') ? 'has-error' : ''}}">
                {!! Form::label('passenger_count', 'Passenger Count: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('passenger_count', 1, ['class' => 'form-control']) !!}
                {!! $errors->first('passenger_count', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            

            <!--div class="form-group {{ $errors->has('paid_date') ? 'has-error' : ''}}">
                {!! Form::label('paid_date', 'Paid Date: ', ['class' => 'col-sm-3 control-label']) !!}

                <div class="col-sm-4">

                <div id="dp-ex-3" class="input-group date" data-auto-close="true"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                    <input class="form-control" type="text" name="paid_date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
                </div>
            </div-->
            
             <div class="form-group {{ $errors->has('contact_name') ? 'has-error' : ''}}">
                {!! Form::label('contact_name', 'Contact Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('contact_name', null, ['class' => 'form-control']) !!}
                {!! $errors->first('contact_name', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('contact_phone') ? 'has-error' : ''}}">
                {!! Form::label('contact_phone', 'Contact Phone: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('contact_phone', null, ['class' => 'form-control']) !!}
                {!! $errors->first('contact_phone', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
           
            <div class="form-group {{ $errors->has('next_of_kin') ? 'has-error' : ''}}">
                {!! Form::label('next_of_kin', 'Next Of Kin: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('next_of_kin', null, ['class' => 'form-control']) !!}
                {!! $errors->first('next_of_kin', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('next_of_kin_phone') ? 'has-error' : ''}}">
                {!! Form::label('next_of_kin_phone', 'Next Of Kin Phone: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('next_of_kin_phone', null, ['class' => 'form-control']) !!}
                {!! $errors->first('next_of_kin_phone', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
             
             <hr>  

             ADD More Passengers 
                
                <table border="0" cellspacing="2">
    <tr class="eachField">
        <td style="width:200px;" align="right"> 
            <td>
                <!-- <input type="text" id="current Name" value="" /> -->
            </td>
        </td>
    </tr>
    
    <tr>
        <td align="right"></td>
        <td>
            <input type="button" id="add" value="Add" />
            <input type="button" id="del" value="Del" />
        </td>
    </tr>
    <tr>
        <td style="height:3px" colspan="2"></td>
    </tr>
    <tr style="background-color: #383838">
        <td></td>
    </tr>
    <tr></tr>
    <tr>
        </div>
        </div>
        </td>
    </tr>
</table>

<br><br><br>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-3">
                        {!! Form::submit('Book', ['class' => 'btn btn-primary form-control']) !!}
                    </div>
                </div>
                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        <div class="col-md-3 col-sm-5">

           <div class="portlet">

            <div class="portlet-header">

              <h3>
                <i class="fa fa-reorder"></i>
                Trip Details
              </h3>

            </div> <!-- /.portlet-header -->

            <div class="portlet-content">

              <dl>
                <dt>Name</dt>
                <dd><a href="{{ url('trips', $trip->id) }}" >{{ $trip->name }}</a></dd>
                <dt>Departure Time</dt>
                <dd> {{ $trip->departure_time }} </dd>
                
                <dt>Fare</dt>
                <dd> {{ $trip->fare }} </dd>

              </dl>

              

            </div> <!-- /.portlet-content -->

          </div>

        </div>

      </div> <!-- /.row -->   

  <script src="{{ url('bckend/js/libs/jquery-1.10.1.min.js') }}"></script>

      <script>
      var more = 1;
      $('#add').click(function () {
        var noo = $('.eachField').length
        console.log(noo+1);
    var table = $(this).closest('table');
    if (table.find('input:text').length < 7) {
        table.append('<tr class="eachField"><td style="width:200px;" align="right">Name&nbsp;&nbsp;&nbsp; <td> <input type="text" id="current Name" name="psg'+noo+'" value="" /> </td></tr>');
        more+1;
    }
});
$('#del').click(function () {

    var noo = $('.eachField').length
    console.log(noo-1);
    var table = $(this).closest('table');
    if (table.find('input:text').length > 1) {
        table.find('input:text').last().closest('tr').remove();
    }
});
</script>

@endsection