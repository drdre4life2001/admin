@extends('layouts.master')

@section('content')

    <div class="content-header">
        <h2 class="content-header-title">Edit Booking </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('bookings') }}">Bookings </a></li>
          <li class="active">Edit Booking </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">

                {!! Form::model($booking, [
                    'method' => 'PATCH',
                    'url' => ['bookings', $booking->id],
                    'class' => 'form-horizontal'
                ]) !!}

                            <div class="form-group {{ $errors->has('date') ? 'has-error' : ''}}">
                {!! Form::label('date', 'Departure Date: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                  <div id="dp-ex-3" class="input-group date" data-auto-close="true"  data-date-format="yyyy-mm-dd" data-date-autoclose="true">
                    <input class="form-control" type="text" name="date" value="{{ $booking->date }}">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
                   
                {!! $errors->first('date', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
          
            <div class="form-group {{ $errors->has('unit_cost') ? 'has-error' : ''}}">
                {!! Form::label('unit_cost', 'Unit Cost: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('unit_cost', null, ['class' => 'form-control']) !!}
                {!! $errors->first('unit_cost', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('final_cost') ? 'has-error' : ''}}">
                {!! Form::label('final_cost', 'Final Cost: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('final_cost', null, ['class' => 'form-control']) !!}
                {!! $errors->first('final_cost', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <!--div class="form-group {{ $errors->has('booking_code') ? 'has-error' : ''}}">
                {!! Form::label('booking_code', 'Booking Code: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('booking_code', null, ['class' => 'form-control']) !!}
                {!! $errors->first('booking_code', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div-->
          

            <div class="form-group {{ $errors->has('contact_phone') ? 'has-error' : ''}}">
                {!! Form::label('contact_phone', 'contact Phone: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('contact_phone', null, ['class' => 'form-control']) !!}
                {!! $errors->first('contact_phone', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


            <div class="form-group {{ $errors->has('contact_name') ? 'has-error' : ''}}">
                {!! Form::label('contact_name', 'Contact Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('contact_name', null, ['class' => 'form-control']) !!}
                {!! $errors->first('contact_name', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('next_of_kin') ? 'has-error' : ''}}">
                {!! Form::label('next_of_kin', 'Next Of Kin: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('next_of_kin', null, ['class' => 'form-control']) !!}
                {!! $errors->first('next_of_kin', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('next_of_kin_phone') ? 'has-error' : ''}}">
                {!! Form::label('next_of_kin_phone', 'Next Of Kin Phone: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('next_of_kin_phone', null, ['class' => 'form-control']) !!}
                {!! $errors->first('next_of_kin_phone', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-3">
                        {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
                    </div>
                </div>
                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['bookings', $booking->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<i class="fa fa-times"></i> Delete Booking', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                {!! Form::close() !!}
            </li>
            <li>
              <a href="{{ url('bookings') }}">
                <i class="fa fa-bars"></i> 
                List Bookings
              </a>
            </li>
            <li class="">
              <a href="{{ url('bookings/create') }}">
                <i class="fa fa-plus"></i> 
                Add Booking
              </a>
            </li>
          </ul>

        </div>

      </div> <!-- /.row -->   

@endsection