@extends('layouts.master')

@section('content')

    <style type="text/css">
        #holder{
            height:{{ $booking['trip']['bus_type']['holder_height'] }}px;
            width:{{ $booking['trip']['bus_type']['holder_width'] }}px;
            padding: 71px 59px;
            background-color:#F5F5F5;
            /*border:1px solid #A4A4A4;*/
            margin-left:10px;
            background-image:url("{{ asset('bckend/img/bus_bg.png') }}");
            background-size:     cover;                      /* <------ */
            background-repeat:   no-repeat;
            background-position: center center;
        }
        #place {
            position:relative;
            margin:7px;
        }
        #place a{
            font-size:0.6em;
        }
        #place li
        {
            list-style: none outside none;
            position: absolute;
        }
        #place li:hover
        {
            background-color:yellow;
        }
        #place .seat{
            background:url("{{ asset('bckend/img/available_seat_img.gif') }}") no-repeat scroll 0 0 transparent;
            height:33px;
            width:33px;
            display:block;
        }

        #place .no-seat{
            /*background:url("{{ asset('bckend/img/available_seat_img.gif') }}") no-repeat scroll 0 0 transparent;*/
            height:33px;
            width:33px;
            display:block;
        }


        #place .selectedSeat
        {
            background-image:url('{{ asset('bckend/img/booked_seat_img.gif') }}');
        }
        #place .selectingSeat
        {
            background-image:url("{{asset('bckend/img/selected_seat_img.gif') }}");
        }
        #place .row-3, #place .row-4{
            margin-top:10px;
        }
        #seatDescription{
            padding:0px;
            margin-left: 30px;
        }
        #seatDescription li{
            verticle-align:middle;
            list-style: none outside none;
            padding-left:35px;
            height:35px;
            float:left;
        }
    </style>



    <div class="container">
        <h1>Booking</h1>

        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}">Dashboard</a></li>
            <li class="active">{{ $page_title }} </li>
            <li><a  href="{{ url('daily-trips') }}">Daily Trips</a></li>
            <li><a  href="{{ url('bookings/status/PAID') }}">Paid Bookings</a></li>
            <li><a  href="{{ url('quick-book') }}">Quick Book</a></li>
            <li><a href="{{ url('bookings') }}">Bookings </a></li>
            <li><a  href="{{ url('trips/search') }}">Book another Ticket</a></li>

        </ol>

        <br><br>

        <div class="alert alert-success hide" id="paidAlertDiv">
            Booking has been marked as PAID. <!--You can now print the Ticket-->
        </div>
        @if ($booking['child'] == 0 )
            <input type ="hidden" id="rt" value="{{'0'}}">
        @else
            <input type ="hidden" id="rt" value="{{'1'}}">
        @endif
        <div class="col-md-8 col-sm-8">

            <div class="portlet">

                <div class="portlet-header">

                    <h1>
                        <i class="fa fa-bus"></i>
                        <span>
                Booking Code: <span id="bookingcode">{{ $booking['booking_code'] }}</span>
                </span>
                    </h1>

                </div> <!-- /.portlet-header -->

                <div class="portlet-content" class="col-sm-12">
                    <div class="clearfix"></div>

                    <div class="row">

                        <div class="col-md-4" id= "dateDiv">
                            <dl>
                                <!-- change made by laolu -->
                                <dt>Booking Date</dt>

                                <dd name="bookingdate"> {{ date('D, d/m/Y h:iA', strtotime($booking['created_at'])) }} </dd>
                                <!-- change made by laolu -->


                                <dt>Departure Date</dt>
                                <dd name="departureDate"> {{ date('D, d/m/Y', strtotime($booking['date'])) }} </dd>

                                @if($booking['child'] == 1)
                                    <dt>Return Date</dt>
                                    <dd name="returnDate"> {{ date('D, d/m/Y', strtotime($booking['return_date']))  }} </dd>
                                @endif

                                <dt>STATUS</dt>
                                <dd><span id='statusField' name="statusField" class="label label-warning">{{ $booking['status'] }}</span> </dd>
                                <br/><br/>
                            </dl>
                        </div>

                        <div class="col-md-4" id="costDiv">
                            <dl>
                                <dt>Num of Passengers </dt>
                                <dd id="nop" name="nop">{{ $booking['passenger_count'] }}</dd>

                                <dt>Unit Cost</dt>
                                <dd>&#8358;{{ number_format($booking['unit_cost']) }}</dd>




                                @if(!empty($booking['luggage_cost']))
                                    <dt>Luggage Cost</dt>
                                    <dd name="luggcost">&#8358;{{ number_format($booking['luggage_cost']) }}</dd>
                                @endif


                                <dt>Final Cost</dt>
                                <dd name="finalcost">&#8358;{{ number_format($booking['final_cost']) }}</dd>

                                @if(isset($booking['luggage_weight']))
                                    <dt>Luggage Weight</dt>
                                    <dd name="luggage_weight"> {{ $booking['luggage_weight'] }}kg</dd>
                                @endif

                                @if($booking['status'] == 'PAID')
                                    <dt>Payment Date</dt>
                                    <dd name="paymentdate"> <?php echo date('D d/m/Y h:iA', strtotime($booking['paid_date'])) ?></dd>
                                @endif
                            </dl>
                        </div>


                        <div class="col-md-4" id="tripDiv">
                            <h4>Trip Details</h4>
                            <dl>
                                <dt>From:</dt>
                                <dd name="from">{{ $booking['trip']['sourcepark']['name'] }}</dd>

                                <dt>To:</dt>
                                <dd name="to">{{ $booking['trip']['destpark']['name'] }}</dd>


                                <dt>Trip Type:</dt>
                                <dd>{{ ($booking['child'] )?"Round Trip":"One Way" }}</dd>


                                <dt>Bus type</dt>
                                <dd name="bustype">{{ $booking['trip']['bus_type']['name'] }}</dd>

                                <dt>Bus Features</dt>
                                <dd>

                                    @if($booking['trip']['ac'])
                                        <span<i class="fa fa-check-square-o font-green-jungle"></i>  AC</span>
                                    @else
                                        <span><i class="fa fa-times-circle font-red-thunderbird"></i>  AC</span>
                                    @endif

                                    &nbsp;&middot;&nbsp;


                                </dd>
                                </span<i></dd></dl></div></div>

                    <hr>

                    <div class="col-md-6">

                        <h4>Passenger Details</h4>

                        <dt>Passenger Name</dt>
                        <dd name="contactname">{{ $booking['contact_name'] }}</dd>

                        <dt>Passenger Phone</dt>
                        <dd name="phone">{{ $booking['contact_phone'] }}</dd>


                        @if(isset($booking['contact_email']))
                            <dt>Passenger Email</dt>
                            <dd name="email">{{ $booking['contact_email'] }}</dd>
                        @endif


                        @if(isset($booking['contact_address']))
                            <dt>Passenger Address</dt>
                            <dd name="contactaddress">{{ $booking['contact_address'] }}</dd>
                        @endif

                    <!--  <dt>Gender</dt>
                <dd name="gender"></dd>
                -->
                        <dt>Next of Kin Name</dt>
                        <dd name="nextofkin">{{ $booking['next_of_kin'] }}</dd>

                        <dt>Next of Kin Phone</dt>
                        <dd name="nextofkinphone">{{ $booking['next_of_kin_phone'] }}</dd>

                        <dt>Other Passsenger(s):</dt>
                        @for($i=0; $i<count($booking['passengers']); $i++)
                            @if($booking['passengers'][$i]['age'] < 10)
                                <dd> {{ $booking['passengers'][$i]['name']. ' | '.$booking['passengers'][$i]['gender'] }} (I)</dd>
                            @else
                                <dd> {{ $booking['passengers'][$i]['name']. ' | '.$booking['passengers'][$i]['gender'] }}</dd> @endif
                        @endfor


                    </div>


                    <br style="clear:both;" />


                    <hr/>

                    @if(isset($booking['status']) && $booking['status'] != 'CANCELLED')
                        <div id="bookedSeats">
                            <h4>Chosen seat(s):</h4>  <span id="bkdSeats">Seat {{ $booked_seats }}</span>
                        </div>
                    @endif

                    <div id="holder"  class="col-md-9">
                        <ul  id="place">

                        </ul>
                    </div>

                </div> <!-- /.portlet-content -->

            </div>
        </div>

        <div class="col-md-3 col-sm-3">


            @if($booking['status'] == 'PENDING')
                <div class="portlet">
                    <div class="portlet-header">
                        <div id="updateStatusDiv"></div>
                        <h3>
                            <i class="fa fa-reorder"></i>
                            Mark as Paid
                        </h3>
                    </div> <!-- /.portlet-header -->

                    <div class="portlet-content">

                        <!-- AJAX FORM -->
                        <form action="{{ route('update-status') }}"  method="post">
                            <input type="hidden" name="status" value="PAID">
                            <input type="hidden" name="booking_id" value="{{ $booking['id'] }}">
                            <div class="form-group {{ $errors->has('payment_method_id') ? 'has-error' : ''}}">
                                {!! Form::label('payment_method_id', 'Payment Method: ', ['class' => 'col-sm-12 control-label']) !!}
                                <div class="col-sm-12">
                                    {!! Form::select('payment_method_id', $payment_methods, null, ['class' => 'form-control','required'=>'required']) !!}
                                    {!! $errors->first('payment_method_id', '<span class="parsley-error-list">:message</span>') !!}
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('receiptNO') ? 'has-error' : ''}}" id="receipt">
                                {!! Form::label('receiptNo', 'ReceiptNo: ', ['class' => 'col-sm-12 control-label']) !!}
                                <div class="col-sm-12">
                                    <input type="text" id="receiptNo" name="receiptNo" class="form-control" maxlength="50">
                                    {!! $errors->first('receiptNo', '<span class="parsley-error-list">:message</span>') !!}
                                </div>
                            </div>

                            <br style="clear: both;" /><br style="clear: both;" />
                            <input type="hidden" id="SubBtn" name="seat" value="{{$booked_seats}}">

                            <input type="hidden" id="SubBtn" name="_token" value="<?php echo csrf_token(); ?>">



                            <input type="submit" class="btn btn-primary" value="Mar as PAID and send sms" id="btnSub">
                        </form>
                    </div>
                </div>
            @endif



            @if($booking['status'] == 'PENDING')
                <div class="portlet" id="cancelDiv">
                    <div id="updateStatusDiv"></div>
                    <div class="portlet-content">
                        {{--<input type="submit" class="btn btn-primary" value="Book on Hold" id="btnBookHold">--}}
                    </div>
                </div>
            @endif

            <div class="portlet">
                <div id="response"></div>
                <div class="portlet-content">
                    <div id="updateStatusDiv"></div>
                    {{--<input type="submit" class="btn btn-primary" value="Send SMS" id="btnResend">--}}
                </div>           <?php


                $msg ="" ;




                ?>

                {{--<form action="" method="post"data-toggle="validator"name="form" role="form" id="submitter">--}}
                {{--<input type="hidden" name="total_revenue" value=" &#8358;">--}}
                {{--<input type="hidden" name="total_expenditure" value=" &#8358;">--}}
                {{--<input type="hidden" name="net_revenue"  value=" &#8358;">--}}

                {{--<div><button type="submit" class="btn-primary">Send SMS</button>--}}
                {{--</div>--}}

                {{--</form>--}}



            </div>
            <div class="portlet" id="cancelDiv">

                <div class="portlet-content">
                    @if($booking['status'] == 'PAID')
                        <a href="{{  url('ticket/'.$booking['id']) }}" target="_blank" class="btn btn-block btn-success">
                            <i class="fa fa-print"></i>
                            Print Ticket
                        </a>
                        <a href="{{ route('cancel-booking', $booking['booking_code']) }}" class="btn btn-block btn-primary">
                            Cancel Booking
                        </a>
                    @elseif($booking['status'] != 'CANCELLED')
                    <form action="{{ route('cancel-booking', $booking['booking_code']) }}"  method="post">
                        <!-- <a href="{{ route('cancel-booking', $booking['booking_code']) }}" class="btn btn-block btn-primary" id="cancelBtn"> -->
                        <input type="hidden" id="cancelBtn" name="_token" value="<?php echo csrf_token(); ?>">
                            <input type="hidden" name="status" value="CANCELLED">
                            <input type="hidden" name="booking_id" value="{{ $booking['id'] }}">
                            <input type="submit" class="btn btn-primary" value="Cancel Booking" id="cancelBtn">


                        <!-- </a> -->
                    @endif
                    </form>
                </div>

            </div>

            <div class="portlet">

                <div class="portlet-header">
                    <h3>
                        <i class="fa fa-reorder"></i>
                        Actions
                    </h3>
                </div> <!-- /.portlet-header -->

                <div class="portlet-content">
                    <!-- <a href="#" class="btn btn-danger">ADD Booking Notes</a> -->

                    <a type="button" class="btn btn-primary btn-block" href="{{ url('bookings/' . $booking['id'] . '/edit') }}"> Edit passenger details </a>

                    <button type="button" class="btn btn-default btn-block " data-toggle="modal" data-target="#myModal"> Add Booking Notes </button>
                </div> <!-- /.portlet-content -->

            </div>


        </div>

        <div id="showNote">
            <div class="col-md-6 col-sm-8">
                <div class="portlet">

                    <div class="portlet-header">

                        <h3>
                            <i class="fa fa-reorder"></i>
                            Notes attached to this Booking
                        </h3>

                    </div> <!-- /.portlet-header -->

                    <div class="portlet-content">


                        <div class="table-responsive">

                            <table
                                    class="table table-striped table-bordered table-hover table-highlight table-checkable"
                                    data-provide="datatable"
                                    data-display-rows="10"
                                    data-info="true"
                                    data-search="true"
                                    data-length-change="true"
                                    data-paginate="true"
                            >
                                <thead>
                                <tr>
                                    <th>S.No</th><th>User_id</th><th>Notes</th><th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                {{-- */$x=0;/* --}}
                                @foreach($notes as $item)
                                    {{-- */$x++;/* --}}
                                    <tr>
                                        <td>{{ $x }}</td>
                                        <td>{{ $item->user->first_name }}</td>
                                        <td>{{ $item->notes }}</td>
                                        <td>{{ $item->created_at }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div> <!-- /.table-responsive -->
                    </div> <!-- /.portlet-content -->

                </div> <!-- /.portlet -->
            </div> <!-- /.col -->
        </div>
        <div class="col-md-5 col-sm-8">
            <div class="portlet">
                <div class="portlet-header">
                    <h3>
                        <i class="fa fa-reorder"></i>
                        Online Transactions Attempts
                    </h3>
                </div> <!-- /.portlet-header -->
                <div class="portlet-content">
                    <div class="table-responsive">
                        <table
                                class="table table-striped table-bordered table-hover table-highlight table-checkable"
                                data-provide="datatable"
                                data-display-rows="10"
                                data-info="true"
                                data-search="true"
                                data-length-change="true"
                                data-paginate="true"
                        >
                            <thead>
                            <tr>
                                <th>S.No</th><th>Status</th><th>Response</th><th>Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            {{-- */$x=0;/* --}}

                            </tbody>
                        </table>
                    </div> <!-- /.table-responsive -->


                </div> <!-- /.portlet-content -->

            </div> <!-- /.portlet -->
        </div> <!-- /.col -->
    </div>

    <div class = "modal fade" id="myModal" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
        <div class = "modal-dialog">
            <div class = "modal-content">
                <div class = "modal-header">
                    <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                        &times;
                    </button>
                    <h4 class = "modal-title" id = "myModalLabel">
                        Add Booking note
                    </h4>
                </div>
                <form action="{{ route('booking-note') }}" method="post" id="bookingNotes">
                    <div class = "modal-body" id="contentArea">
                        <input type="hidden" id="SubBtn" name="_token" value="<?php echo csrf_token(); ?>">
                        <input type="hidden" name="booking_id" value="{{ $booking['id'] }}">
                        Notes: <textarea rows="7" cols="50" name="notes" class="form-control"></textarea>
                    </div>
                    <div class = "modal-footer">
                        <button type = "button" class = "btn btn-default" data-dismiss = "modal">
                            Close
                        </button>
                        <button type = "submit" id="NoteSub" class = "btn btn-primary">
                            Submit changes
                        </button>
                </form>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->






    <script type="text/javascript">
        $(function () {
            var settings = {
                rows: {{ $booking['trip']['bus_type']['no_of_rows'] }},
                cols: {{ $booking['trip']['bus_type']['no_of_columns'] }},
                rowCssPrefix: 'row-',
                colCssPrefix: 'col-',
                seatWidth: {{ $booking['trip']['bus_type']['seat_width'] }},
                seatHeight: {{ $booking['trip']['bus_type']['seat_height'] }},
                seatCss: 'seat',
                selectedSeatCss: 'selectedSeat',
                selectingSeatCss: 'selectingSeat'
            };
            /* laolu starts here */
            var bkArray = [];
            var bookedSeats = $("#bkdSeats").text();
            // get the status tag
            var bookingDet = $("#statusField").text();

            // if contains comma
            if(bookedSeats.indexOf(",") > -1)
            {
                // split into
                bkArray = bookedSeats.split(",");
            }
            else
            {
                bkArray[0] = bookedSeats;
            }
            /* laolu ends here*/
            // var init = function (reservedSeat, noSeat, bookingDet) {
            // laolu starts edit from here
            var init = function (reservedSeat, noSeat, bookingDet) {

                var no_of_seats = {{ $booking['trip']['bus_type']['no_of_seats'] }};

                var str = [], seat, seatNo = 0, className;

                if(no_of_seats == 30)
                {
                    settings.seatWidth = settings.seatWidth/1.8;
                    settings.seatHeight = settings.seatHeight/1.2;

                }

                var str = [], seat, seatNo = 0, className;


                //incldue the right seat...
                @include('bookings.includes.'.$booking['trip']['bus_type']['no_of_seats'].'-seater-layout');


                $('#place').html(str.join(''));
                //libra 15 seats...
            };

            //case I: Show from starting
            //init();

            //Case II: If already booked
            var bookedSeats = [{{ $booked_seats }}];
            // alert(bkArray.length);
            for(var i=0; i <bkArray.length; i++)
            {
                var index = bkArray.indexOf(bkArray[i]);
                if (index > -1)
                {
                    bkArray.splice(index, 1);
                }
            }

            if(bookingDet == 'CANCELLED')
            {
                init(bkArray, [{{ $booking['trip']['bus_type']['no_seats'] }}],bookingDet);
            }
            else
            {
                init(bookedSeats, [{{ $booking['trip']['bus_type']['no_seats'] }}],bookingDet);
            }
        });


        function checkChosenSeats(str){
            var passenger_count = $("#passenger_count").val();
            if(str.length == passenger_count){
                $('#submitBtn').removeClass('disabled');
            }else{
                $('#submitBtn').addClass('disabled');
            }

        }
    </script>
    <script>



        // payment method wahala

        $(document).ready(function(){
            $("#receipt").hide();

            var rt = $("#rt").val();

            var landline ="+2348074490452";

            var bookingcode = $("#bookingcode").html();
            var busNo = ""
            var phone = $("dd[name=phone]").text();
            var fromArr = $("dd[name=from]").text().split("[");
            var from = fromArr[0];
            var toArr = $("dd[name=to]").text().split("[");
            var to = toArr[0];
            var bookingdate = $("dd[name=bookingdate]").text();
            var departureDate = $("dd[name=departureDate]").text();
            var returnDate = $("dd[name=returnDate]").text();
            var status = $("span[name=statusField]").html();
            var finalcost = $("dd[name=finalcost]").text();
            finalcost = finalcost.substr(1);
            var bustype = $("dd[name=bustype]").text();
            var contactname = $("dd[name=contactname]").text();
            var bookedSeats = $("#bkdSeats").html();

            var gender = $("dd[name=gender]").text().charAt(0).toLowerCase();
            var op = "{{ session('operator_name') }}";
            var title="";
            var nop = $("dd[name=nop]").text();
            if(gender == 'm')
                title = "Sir";
            else
                title = "Ma";

            // $("#btnSub").on('click', function(){
            //   window.location.reload();
            // });

            $("#btnBookHold").on('click', function(){


                $("#updateStatusDiv").addClass('alert alert-info').html('Please wait....');

                var msg = op + " book on hold: "+" Book Code: "+bookingcode+ " from: "+" "+
                    from +"to: "+ to +
                    ". Name: "+contactname+
                    ". Bus:"+busNo+
                    ". Seat:"+bookedSeats+
                    ". Passengers: "+nop+
                    ". Final Cost: NGN "+ finalcost+" "+
                    ". Contact: "+ landline;
                var rtbuild = "Return Date "+returnDate;
                var rtStatus = (rt == 1) ? rtbuild: " ";
                msg+=rtStatus;



                $.ajax({
                    url: '/',
                    dataType: 'text',
                    type: 'GET',
                    contentType: 'application/x-www-form-urlencoded',
                    data: {"phone":phone,"msgg":msg},
                    success: function (rslts, textStatus, jQxhr)
                    {
                        if(rslts == 200)
                        {
                            $("#updateStatusDiv").addClass('alert alert-success').html('SMS Sent');
                            $("#updateStatusDiv").delay(5000).fadeOut();
                        }
                        else
                        {
                            $("#updateStatusDiv").addClass('alert alert-danger').html('SMS not sent');
                            $("#updateStatusDiv").delay(5000).fadeOut();
                        }
                        window.location.reload();
                    },
                    error: function (jqXhr,textStatus, errorThrown)
                    {
                        $("#updateStatusDiv").addClass('alert alert-danger').html('SMS not sent');
                        $("#updateStatusDiv").delay(5000).fadeOut();
                    }
                });
            });




            $("#btnResend").on('click', function(){

                // get status
                var status = $("#statusField").html();
                $("#updateStatusDiv").addClass('alert alert-info').html('Please wait....');

                if(status == "PENDING")
                {

                    var msg = op + " book on hold: "+" Book Code: "+bookingcode+ " from: "+" "+
                        from +"to "+ to +
                        ". Name: "+contactname+
                        ". Bus: "+busNo+
                        ". Seat:"+bookedSeats+
                        ". Passengers: "+nop+
                        ". Final Cost: NGN"+ finalcost+" "+
                        ". Contact: "+ landline;
                    var rtbuild = "Return Date "+returnDate;
                    var rtStatus = (rt == 1) ? rtbuild: " ";
                    msg+=rtStatus;
                    $.ajax({
                        url: '{{ url("send-sms") }}',
                        dataType: 'text',
                        type: 'GET',
                        contentType: 'application/x-www-form-urlencoded',
                        data: {"phone":phone,"msgg":msg},
                        success: function (rslts, textStatus, jQxhr)
                        {
                            if(rslts == 200)
                            {

                                $("#updateStatusDiv").addClass('alert alert-success').html('SMS Sent');
                                $("#updateStatusDiv").delay(5000).fadeOut();
                            }
                            else
                            {
                                $("#updateStatusDiv").addClass('alert alert-danger').html('SMS not sent');
                                $("#updateStatusDiv").delay(5000).fadeOut();
                            }
                            window.location.reload();
                        },
                        error: function (jqXhr,textStatus, errorThrown)
                        {
                            $("#updateStatusDiv").addClass('alert alert-danger').html('SMS not sent');
                            $("#updateStatusDiv").delay(5000).fadeOut();
                        }
                    });
                }

                else if(status == "PAID")
                {

                    var msg = op + " purchase notice : "+" Book Code: "+bookingcode+ " from: "+" "+
                        from +"to "+ to +
                        ". Name: "+contactname+
                        ". Bus: "+busNo+
                        ". Seat:"+bookedSeats+
                        ". Passengers: "+nop+
                        ". Final Cost: NGN"+ finalcost+" "+
                        ". Contact: "+ landline;
                    var rtbuild = "Return Date "+returnDate;
                    var rtStatus = (rt == 1) ? rtbuild: " ";
                    msg+=rtStatus;

                    $.ajax({
                        url: '{{ url("send-sms") }}',
                        dataType: 'text',
                        type: 'GET',
                        contentType: 'application/x-www-form-urlencoded',
                        data: {"phone":phone,"msgg":msg},
                        success: function (rslts, textStatus, jQxhr)
                        {
                            if(rslts == 200)
                            {
                                $("#updateStatusDiv").addClass('alert alert-success').html('SMS Sent');
                                $("#updateStatusDiv").delay(5000).fadeOut();
                            }
                            else
                            {
                                $("#updateStatusDiv").addClass('alert alert-danger').html('SMS not sent');
                                $("#updateStatusDiv").delay(5000).fadeOut();
                            }
                            window.location.reload();
                        },
                        error: function (jqXhr,textStatus, errorThrown)
                        {
                            $("#updateStatusDiv").addClass('alert alert-danger').html('SMS not sent');
                            $("#updateStatusDiv").delay(5000).fadeOut();
                        }
                    });

                    // CALL mark as paid
                    $('#updateStatus').ajaxForm({
                        beforeSubmit: genPreSubmit,
                        success: function(response){

                            console.log(response);
                            $('#statusField').html('<span class="label label-success">PAID</span> ');
                            $('#flash').html('Booking Status Successfully updated')
                            $('#updateStatusDiv').hide('slow');
                            $('#cancelDiv').hide('slow');
                            $('#paidAlertDiv').removeClass('hide');
                            window.location.reload();
                        },
                        reset: true
                    });
                }
            });

            // on change
            $("#payment_method_id").on('change', function(){
                // get what is method is being used
                var paymtMthd =  $("#payment_method_id").val();
                if(paymtMthd == 3)
                {
                    $("#receipt").show();
                }
                else
                    $("#receipt").hide();

            });

            $('#updateStatus').ajaxForm({
                beforeSubmit: genPreSubmit,
                success: function(response){
                    console.log(response);
                    $('#statusField').html('<span class="label label-success">PAID</span> ');
                    $('#flash').html('Booking Status Successfully updated')
                    $('#updateStatusDiv').hide('slow');
                    $('#cancelDiv').hide('slow');
                    $('#paidAlertDiv').removeClass('hide');

                    location.reload(true);

                },
                reset: true

            });

            $('#bookingNotes').ajaxForm({
                beforeSubmit: genPreSubmit,
                success: function(response){
                    console.log(response);
                    $('#myModal').modal('hide')
                    $('#showNote').html(response);
                    $('#flash').html('Note Added Successfully')

                },
                reset: true
            });

            function genPreSubmit(){

                console.log("We are here....");
                $("#SubBtn").html('please wait...');


            }

            // send cancel sms
            $("#cancelBtn").on('click',function(){note
                // send sms
                var msg = op + " cancel notice: "+" Book Code: "+bookingcode+ " from: "+" "+
                    from +"to "+ to +
                    ". Name: "+contactname+
                    ". Bus: "+busNo+
                    ". Seat:"+bookedSeats+
                    ". Passengers: "+nop+
                    ". Final Cost: NGN "+finalcost+" "+
                    ". Call: "+ landline;
                var rtbuild = "Return Date "+returnDate;
                var rtStatus = (rt == 1) ? rtbuild: " ";
                msg+=rtStatus;

                $.ajax({
                    url: '{{ url("send-sms") }}',
                    dataType: 'text',
                    type: 'GET',
                    contentType: 'application/x-www-form-urlencoded',
                    data: {"phone":phone,"msgg":msg},
                    success: function (rslts, textStatus, jQxhr)
                    {
                        if(rslts == 200)
                        {
                            $("#updateStatusDiv").addClass('alert alert-success').html('SMS Sent');
                            $("#updateStatusDiv").delay(5000).fadeOut();
                        }
                        else
                        {
                            $("#updateStatusDiv").addClass('alert alert-danger').html('SMS not sent');
                            $("#updateStatusDiv").delay(5000).fadeOut();
                        }
                    },
                    error: function (jqXhr,textStatus, errorThrown)
                    {
                        $("#updateStatusDiv").addClass('alert alert-danger').html('SMS not sent');
                        $("#updateStatusDiv").delay(5000).fadeOut();
                    }
                });
            });

        });
    </script>
@endsection