@extends('layouts.master')

@section('content')

    <div class="content-header">
        <h2 class="content-header-title">{{ ucwords(strtolower($page_title)) }} </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li class="active">Bookings </li>
        </ol>
      </div> <!-- /.content-header -->
    
      

      <div class="row">

        <div class="col-md-12 col-sm-12">

          

              <div class="btn-group pull-right">
                  <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                      <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                      <span></span> <b class="caret"></b>
                  </div>
              </div>
              <h4 class="heading-inline pull-right">
          &nbsp;&nbsp;<small>Choose date</small>
          &nbsp;&nbsp;</h4>

              <br/><br/><br/>


          <div class="portlet">
          
           @if(Session::has('flash_message'))
              <div class="alert alert-success">
              <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
              <strong>Well done!</strong> {!! session('flash_message') !!}
            </div>

            @endif

            <div class="portlet-content">           

              <div class="table-responsive">

             <table 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>Booking code</th>
                      <th>From</th>
                      <th>To</th>
                      <th>Travel Time</th>
                      <th>Passenger Count</th>
                      <th>Final Cost</th>
                      <th>Contact Person</th>
                      <th>Contact Phone</th>
                      <th>Status</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($bookings as $item)

                        {{-- */$x++;/* --}}
                        <tr>
                            <td>{{ $item['booking_code'] }}</td>
                            <td>{{ $item['trip']['sourcepark']['name'] }}</td>
                            <td>{{ $item['trip']['destpark']['name'] }}</td>
                            <td>{{ date('D, d/m/Y', strtotime($item['date'])). ' '.$item['trip']['departure_time'] }}</td>
                            <td>{{ $item['passenger_count']}}</td>
                            <td>&#8358;{{ number_format(((Auth::user()->role->name != 'Operator')?$item['final_cost']:$item['final_cost'] - 150)) }}</td>
                            <td>{{ $item['contact_name'] }}</td>
                            <td>{{ $item['contact_phone'] }}</td>
                            <td>{{ $item['status'] }}</td>
                            <td>
                                <a href="{{ url('bookings/' . $item['id'] ) }}" title="View">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-eye"></i></button>
                                </a> 

                                @if(Auth::user()->role->name != 'Operator')
                                <a href="{{ url('bookings/' . $item['id'] . '/edit') }}" title="Edit">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-edit"></i></button>
                                </a> 

                                @endif
                                <!--
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['bookings', $item['id']],
                                    'style' => 'display:inline'
                                ]) !!}
                                    {!! Form::button('<i class="fa fa-times"></i>', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit', 'onclick'=>'ConfirmDelete()']) !!}
                                {!! Form::close() !!}
                                -->
                            </td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div> <!-- /.table-responsive -->
              

            </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

      

      </div> <!-- /.row -->    

<script type="text/javascript">

function updateUrl(name, value){
      window.location.search = jQuery.query.set(name, value);

      return false;
 } 

function refreshReport(){

  console.log($('#select-input').val());
  window.location = '{{ url("trip-schedules") }}/'+$('#park-input').val();


}


$(function() {

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    cb(moment("{{ $start_date }}"), moment("{{ $end_date }}"));

    $('#reportrange').daterangepicker({
        ranges: {
           // 'Next Tomorrow': [moment().add(2, 'days'), moment().add(2, 'days')],
           // 'Tomorrow': [moment().add(1, 'days'), moment().add(1, 'days')],
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
        console.log(picker.startDate.format('YYYY-MM-DD'));
        console.log(picker.endDate.format('YYYY-MM-DD'));
        
        updateUrl('daterange', picker.startDate.format('YYYY-MM-DD')+' '+picker.endDate.format('YYYY-MM-DD'));
    });

});

</script>

@endsection
