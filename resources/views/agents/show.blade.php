@extends('layouts.master')

@section('content')

    <h1>Agent</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>User Id</th><th>Name</th><th>Account Details</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $agent->id }}</td> <td> {{ $agent->user_id }} </td><td> {{ $agent->name }} </td><td> {{ $agent->account_details }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection