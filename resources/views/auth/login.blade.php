@extends('layouts.auth')

@section('content')

<div class="account-body">

      <h3 class="account-body-title">Login.</h3>

      <h5 class="account-body-subtitle">Please sign in to get access.</h5>

      <form class="form account-form" role="form" method="POST" action="{{ url('/login') }}">
        {!! csrf_field() !!}

        <div class="form-group">
          <label for="login-username" class="placeholder-hidden">Email Address</label>
          <input type="text" class="form-control" name="email" id="login-username" value="{{ old('email') }}" placeholder="Email Address" tabindex="1">
            @if ($errors->has('email'))
                <span class="parsley-error-list">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif

        </div> <!-- /.form-group -->

        <div class="form-group">
          <label for="login-password" class="placeholder-hidden">Password</label>
          <input type="password" class="form-control" name="password" id="login-password" placeholder="Password" tabindex="2">
            @if ($errors->has('password'))
                <span class="parsley-error-list">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div> <!-- /.form-group -->

        <div class="form-group clearfix">
          <div class="pull-left">         
            <label class="checkbox-inline">
            <input type="checkbox" class="" value="" tabindex="3">Remember me
            </label>
          </div>

          <div class="pull-right">
            <a href="{{ url('/password/reset') }}">Forgot Password?</a>
          </div>
        </div> <!-- /.form-group -->

        <div class="form-group">
          <button type="submit" class="btn btn-primary btn-block btn-lg" tabindex="4">
            Login &nbsp; <i class="fa fa-play-circle"></i>
          </button>
        </div> <!-- /.form-group -->

      </form>


    </div> <!-- /.account-body -->

    <!--div class="account-footer">
      <p>
      Don't have an account? &nbsp;
      <a href="{{ url('/register') }}" class="">Create an Account!</a>
      </p>
    </div> <!-- /.account-footer -->


@endsection
