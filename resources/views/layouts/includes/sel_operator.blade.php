@if(Session::has('operator_id')) 

                 <h4 style="color: white">Currently working on:</h4> 
                 <div class="list-group">  

                  <a class="list-group-item" href="{{ url('operators') }}">
                    
                    @if(Auth::user()->role->name != 'Operator')
                      <h3 class="pull-right"><i class="fa fa-edit"></i></h3>
                    @endif

                        <h4 class="list-group-item-heading">
                          <img src="{{ asset('logos/'.session('operator_logo')) }}" width="40px" />
                          {{ session('operator_name') }}
                        </h4>
                        
                        
                      </a>
                </div>

                @endif