$(function () {
	
    if (!$('#donut-chart').length) { return false; }

	donut ();

	$(window).resize (target_admin.debounce (donut, 325));

});

function donut () {
	$('#donut-chart').empty ();

	Morris.Donut({
        element: 'donut-chart',
        data: [
            {label: 'Paid Bookings', value: 35 },
            {label: 'Pending Bookings', value: 40 },
            {label: 'Cancelled Bookings', value: 25 },
            
        ],
        colors: target_admin.layoutColors,
        hideHover: true,
        formatter: function (y) { return y + "%" }
    });
}