<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'last_name', 'first_name', 'email', 'password', 'phone', 'address', 'active', 'role_id', 'operator_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function role(){
        return $this->belongsTo('App\Models\Role', 'role_id');
    }

    public function operator(){
        return $this->belongsTo('App\Models\Operator', 'operator_id');
    }

    public function logs(){
        return $this->hasMany('App\Models\UserLog', 'user_id');
    }
}
