<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Booking extends Model
{
    protected $fillable = ['trip_id', 'date', 'passenger_count', 'unit_cost', 'final_cost', 'payment_method_id', 'parent_booking_id', 'status', 'paid_date', 'booking_code', 'contact_name', 'contact_phone', 'next_of_kin', 'next_of_kin_phone'];

    public function trip(){
        return $this->belongsTo('App\Models\Trip');
    }

}
