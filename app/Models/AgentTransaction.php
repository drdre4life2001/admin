<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgentTransaction extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'agent_transactions';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['agent_id', 'type', 'value', 'amount_paid', 'approved'];

     public function agent(){
        return $this->belongsTo('App\Models\Agent');
        
    }

}
