<?php

namespace App\Libraries;

class Utility
{
    
    /**
     * This is the template for all responses the api will serve
     * @var array
     */
    public static $response = ['status'=> 404, 'data' => [], 'message' => '' ];

    /**
     * This sets default parameters for requests
     * @param Request $request this is the request object
     * @param Array $default wanted fields tied to thier defaults
     * @return  Request $request this is the refined request object
     */
    public static function setDefaults($request, $default)
    {

        foreach ($default as $key => $value) {
            $request[$key] = ( is_null( $request[$key] ) ) ? $default[$key] : $request[$key];
        }
        return $request;
    }

    /**
     * This sets default parameters for requests
     * @param Request $request this is the request object
     * @param Array $default wanted fields tied to thier defaults
     * @return  Request $request this is the refined request object
     */
    public static function checkParameters($request, $required_params)
    {
        $missing = array();
        Utility::$response['status'] = 200;

        foreach ($required_params as $param) {
            if( is_null( $request[$param] ) )
                $missing[] = $param;   
            
        }
        
        if(!empty($missing)){
            Utility::$response['status'] = 404;
            Utility::$response['message'] = trans('app.missing-req-params', array('missing-params'=>implode(",", $missing)));

            return Utility::$response;

        }else
            return Utility::$response;
    }

}
