<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Booking;
use App\Models\Customer;

use Carbon\Carbon;
use Mail;

class SendFeedbackEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendFeedbackEmails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to send Emails to passengers to review the trip after completion';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $completed_trip = Booking::selectRaw('bookings.*')->leftjoin('customers_feedback', 'customers_feedback.booking_id','=', 'bookings.id')
                                ->where('departure_date', '<', Carbon::now())
                                ->where('status', 'PAID')
                                ->whereNull('customers_feedback.booking_id')
                                ->get();

        foreach ($completed_trip as $trip) {

            $content =" We would love to know how your last trip went";

            $link = env("CTS_URL")."/feedback/".$trip->id;
            $data = ['trip' => $trip, 'content' => $content, 'link' => $link];

            Mail::queue('emails.feedback', $data, function ($message) use ($data){
                    $message->from('noreply@bus.com.ng','Bus.com.ng');
                    $message->to($data['trip']->contact_email);
                    $message->subject("What Do you think about your Trip?");
            });
        }


    }
}
