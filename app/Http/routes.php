<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('backend.layout.app');
// });

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::controller('online', 'OnlineBookingApiController');
// Route::controller('auth', 'AuthController');


Route::group(['middleware' => ['web']], function () {
    //
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();

	Route::resource('bookings', 'BookingsController');
	Route::match(['get', 'post'], 'update-trip', ['uses' => 'TripsController@UploadTrips', 'as' => 'update-trip']);
    Route::match(['get', 'post'], 'upload-trip', ['uses' => 'TripsController@UplodaTrip', 'as' => 'upload-trip']);

    Route::match(['get', 'post'], 'download-trips', ['uses' => 'TripsController@DownloadTrips',
        'as' => 'download-trips']);
	Route::match(['get', 'post'], 'change-feedback-status/{id}/{type}',
        ['uses' => 'BookingsController@changeFeedBackStatus',
        'as' => 'change-feedback-status']);
	Route::match(['get', 'post'], 'upload-file', ['uses' => 'TripsController@uploadfile', 'as' => 'upload-file']);
	Route::match(['get', 'post'], 'trips/search', ['uses' => 'TripsController@Search', 'as' => 'search']);
	Route::match(['get', 'post'], 'trips/adjust_prices', ['uses' => 'TripsController@AdjustPrices', 'as' => 'adjust-prices']);
	Route::match(['get', 'post'], 'booking/add/{tripid}/{date}', ['uses' => 'BookingsController@AdminBook', 'as' => 'admin-book']);
	Route::match(['get', 'post'], 'booking/agent/{user}', ['uses' => 'BookingsController@AgentBooking', 'as' => 'agent-booking']);
	Route::match(['get', 'post'], 'bookings/status', ['uses' => 'BookingsController@Status', 'as' => 'status']);
	Route::match(['get', 'post'], 'bookings/notes', ['uses' => 'BookingsController@Note', 'as' => 'booking-note']);
	Route::match(['get', 'post'], 'bookings/update-status', ['uses' => 'BookingsController@UpdateStatus', 'as' => 'update-status']);
	Route::match(['get', 'post'], 'bookings/status/{id}', ['uses' => 'BookingsController@Status', 'as' => 'status']);
	Route::match(['get', 'post'], 'booking/cancel/{code}', ['uses' => 'BookingsController@cancel', 'as' => 'cancel-booking']);
	Route::match(['get', 'post'], 'fin-summary', ['uses' => 'BookingsController@finance_summary', 'as' => 'fin-summary']);
	Route::match(['get', 'post'], 'update-trips', ['uses' => 'TripsController@generateTripCodes',
        'as' => 'update-trips']);
    Route::get('/', 'UsersController@dashboard');
    // Route::resource('state', 'StateController');
    Route::resource('roles', 'RolesController');
    Route::resource('trips', 'TripsController');
    Route::resource('states', 'StatesController');
    Route::resource('parks', 'ParksController');
    Route::resource('settings', 'SettingsController');
	Route::resource('userlogs', 'UserLogsController');
	Route::resource('users', 'UsersController');
	Route::resource('operators', 'OperatorsController');
	Route::resource('agents', 'AgentsController');
	Route::resource('devices', 'DevicesController');
	Route::resource('bookings', 'BookingsController');
	Route::resource('chartered-bookings', 'CharteredBookingController');
	Route::match(['get','post'],'feedbacks', 'BookingsController@feedbacks');
    Route::match(['get','post'],'cancel_redundant_bookings', 'BookingsController@cancelRedundantBookings');
    Route::match(['get', 'post'], 'operators/select/{id}', ['uses' => 'OperatorsController@Select', 'as' => 'select']);
	Route::match(['get', 'post'], 'operators/index/{status?}', ['uses' => 'OperatorsController@index', 'as' => 'select']);
});

Route::controller('api', 'ApiController');
Route::controller('schedule', 'ScheduleController');
Route::get('test', 'TestController@index');


Route::group(['middleware' => ['web']], function () {
	Route::resource('cashoffices', 'CashOfficesController');
});

Route::controller('data-upload', 'DataUploadController');
Route::get('/test-mail', function(){
	dd(Mail::send('emails.sample', ['name' => 'Deji Lana'], function ($m){
            $m->from('info@bus.com.ng', 'Bus.com.ng');
            $m->to('me@dejilana.com', 'Deji Lana')->subject('Sample for API!');
      }));
});