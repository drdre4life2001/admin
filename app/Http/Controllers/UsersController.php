<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Booking;
use App\Models\BuscomBooking;
use App\Models\Trip;
use App\Models\CharteredBooking;
use App\Models\Operator;
use App\Models\Role;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Auth;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = User::paginate(100000);

        $page_title = 'users';

        return view('users.index', compact('users', 'page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Add user';

        $roles = [null => '-Choose-'] + Role::lists('name', 'id')->toArray();
        $operators = [null => '-Choose-'] + Operator::lists('name', 'id')->toArray();


        return view('users.create', compact('page_title', 'roles', 'operators'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['last_name' => 'required', 'first_name' => 'required', 'email' => 'required', 'phone' => 'required', 'role_id' => 'required', ]);

        User::create($request->all());

        Session::flash('flash_message', 'User added!');

        return redirect('users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        $page_title = 'View user';
        return view('users.show', compact('user', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        $page_title = 'Edit user';
        $roles = [null => ''] + Role::lists('name', 'id')->toArray();
        $operators = [null => ''] + Operator::lists('name', 'id')->toArray();

        return view('users.edit', compact('user', 'page_title', 'roles', 'operators'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['last_name' => 'required', 'first_name' => 'required', 'email' => 'required', 'phone' => 'required', 'role_id' => 'required', ]);

        $user = User::findOrFail($id);
        $user->update($request->all());

        Session::flash('flash_message', 'User updated!');

        return redirect('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        User::destroy($id);

        Session::flash('flash_message', 'User deleted!');

        return redirect('users');
    }

    public function dashboard(Request $request){

        $end_date = date('Y-m-d H:i:s');
        $start_date = date('Y-m-1');


        if(Auth::user()->role->name == 'Operator'){

            $request->session()->put('operator_id', Auth::user()->operator->id );
            $request->session()->put('operator_name', Auth::user()->operator->name );
            $request->session()->put('operator_logo', Auth::user()->operator->img );

            $trip_ids = Trip::where('operator_id', session('operator_id'))->lists('id')->toArray();
            // $bookings = $bookings;

            $totalCount = $bookings = BuscomBooking::where('created_at', '>=', $start_date.' 00:00:00')
                        ->whereIn('trip_id', $trip_ids)
                        ->where('created_at', '<=', $end_date)
                        ->count();

            $paidCount = $bookings = BuscomBooking::where('created_at', '>=', $start_date.' 00:00:00')
                            ->whereIn('trip_id', $trip_ids)
                            ->where('created_at', '<=', $end_date)
                            ->where('status', 'PAID')
                            ->count();
            $paidSum = $bookings = BuscomBooking::where('created_at', '>=', $start_date.' 00:00:00')
                            ->whereIn('trip_id', $trip_ids)
                            ->where('created_at', '<=', $end_date)
                            ->where('status', 'PAID')
                            ->sum('final_cost');
            $charteredSum = $bookings = CharteredBooking::where('created_at', '>=', $start_date.' 00:00:00')
                            ->where('operator_id', session("operator_id"))
                            ->where('created_at', '<=', $end_date)
                            ->where('status', 'PAID')
                            ->sum('agreed_price');

            // reflection of chartered sum in total
            $paidSum+=$charteredSum;

            $pendCount = $bookings = BuscomBooking::where('created_at', '>=', $start_date.' 00:00:00')
                            ->whereIn('trip_id', $trip_ids)
                            ->where('created_at', '<=', $end_date)
                            ->where('status', 'PENDING')
                            ->count();

            $pendSum = $bookings = BuscomBooking::where('created_at', '>=', $start_date.' 00:00:00')
                            ->whereIn('trip_id', $trip_ids)
                            ->where('created_at', '<=', $end_date)
                            ->where('status', 'PENDING')
                            ->sum('final_cost');

            $op_id = session('operator_id');
             //dd($op_id);
            $bookings = BuscomBooking::with('trip.sourcepark', 'trip.destpark')
                        ->whereIn('trip_id', $trip_ids)
                        ->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->paginate(5)->toArray();
            $bookings = ($bookings['data']);

            // chartered bookings
            $cBookings = CharteredBooking::where('operator_id', session('operator_id'))
                                ->orderBy('created_at', 'desc')->take(5)->get();
            $cBookingsNo = count($cBookings);
        

        }else{


            $totalCount = $bookings = BuscomBooking::where('created_at', '>=', $start_date.' 00:00:00')
                        ->where('created_at', '<=', $end_date)
                        ->count();

            $paidCount = $bookings = BuscomBooking::where('created_at', '>=', $start_date.' 00:00:00')
                            ->where('created_at', '<=', $end_date)
                            ->where('status', 'PAID')
                            ->count();
            $paidSum = $bookings = BuscomBooking::where('created_at', '>=', $start_date.' 00:00:00')
                            ->where('created_at', '<=', $end_date)
                            ->where('status', 'PAID')
                            ->sum('final_cost');
            $charteredSum = $bookings = CharteredBooking::where('created_at', '>=', $start_date.' 00:00:00')
                            ->where('created_at', '<=', $end_date)
                            ->where('status', 'PAID')
                            ->sum('agreed_price');

            // reflection of chartered sum in total
            $paidSum+=$charteredSum;

            $pendCount = $bookings = BuscomBooking::where('created_at', '>=', $start_date.' 00:00:00')
                            ->where('created_at', '<=', $end_date)
                            ->where('status', 'PENDING')
                            ->count();

            $pendSum = $bookings = BuscomBooking::where('created_at', '>=', $start_date.' 00:00:00')
                            ->where('created_at', '<=', $end_date)
                            ->where('status', 'PENDING')
                            ->sum('final_cost');

            $op_id = session('operator_id');
             //dd($op_id);
            $bookings = BuscomBooking::with('trip.sourcepark', 'trip.destpark')
                        ->orderBy('created_at', 'desc')->orderBy('created_at', 'desc')->paginate(5)->toArray();
            $bookings = ($bookings['data']);

            // chartered bookings
            $cBookings = CharteredBooking::where('operator_id', session('operator_id'))
                                ->orderBy('created_at', 'desc')->take(5)->get();
            $cBookingsNo = count($cBookings);


        }

        

        

        
        
        // get the role of the user
        $userRole = Auth::user()->role;
        $roleNo = $userRole->id;
        
        /// get data to be displayed in dashboard

       

        //dd($bookings);
        if ($userRole->name == 'OPERATIONS')
        {
           return view('users.operatordashboard', compact('bookings', 'start_date', 'end_date', 'busesOwnedNo', 'tripsCreatedNo', 'allDriversNo','destNo', 'pendSum', 'totalCount', 'trips', 'charteredSum')); 

        }else if($userRole->name == 'Super User'){

            return view('users.dashboard', compact('bookings', 'start_date', 'end_date', 'busesOwnedNo', 'tripsCreatedNo', 'allDriversNo','destNo', 'pendSum', 'totalCount', 'trips', 'paidCount',
                  'paidSum', 'pendCount','cBookings','cBookingsNo', 'charteredSum')); 

        }

        return view('users.dashboard', compact('bookings', 'start_date', 'end_date', 'paidCount',
                  'paidSum', 'pendCount', 'pendSum', 'totalCount','cBookings','cBookingsNo', 'charteredSum'));
    }

}
