<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Libraries\Utility;
use App\Models\Agent;
use App\Models\AgentTransaction;
use App\Models\Park;
use App\Models\Operator;
use App\Models\Trip;
use App\Models\Booking;
use App\Models\Passenger;
use App\Models\Customer;
use App\Models\CustomerFeedback;
use DB;

class ApiController extends Controller
{

    public function postLogin(Request $request)
    {
        Utility::$response = Utility::checkParameters($request, array('username', 'password'));
        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);


        //Get password of submitted email if it exists in DB
        $agent = Agent::where('username', $request->username)->first();
        if(empty($agent)) {
           Utility::$response['status'] = 404;
           Utility::$response['message'] = trans('auth.no_account');
           return response()->json(Utility::$response);
        }
        // echo($request->password);
        // exit;
        // if($agent->password == $request->passsword)
        if(true)
        {

        	$agent->wallet_balance = $agent->balance;
        	Utility::$response['status'] = 200;
            Utility::$response['data'] = $agent;
            Utility::$response['message'] = trans('auth.success');
            return response()->json(Utility::$response);
        }
        else
        {
            Utility::$response['status'] = 500;
            Utility::$response['message'] = trans('auth.failed');

            return response()->json(Utility::$response);
        }


    }

    public function cancel_redundant_bookings(){
        $bookings = Agent::find();
        $parks = Booking::with('state')->where('active',1)->where('boardable',1)->get();
    }


    public function postGetDashData(Request $request)
    {
        Utility::$response = Utility::checkParameters($request, array('agent_id'));
        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);


        //Get password of submitted email if it exists in DB
        $agent = Agent::find($request->agent_id);

        if(empty($agent)) {
           Utility::$response['status'] = 404;
           Utility::$response['message'] = trans('auth.no_account');

           return response()->json(Utility::$response);
        }

        // dd($agent->password == $request->passsword);

        // if($agent->password == $request->passsword)
        $agent->wallet_balance = $agent->balance;

        //getting dashboard data
        $bookings_values_sum = $agent->transactions()->where('type', 'BOOKING')->sum('value');
        $bookings_paid_sum = $agent->transactions()->where('type', 'BOOKING')->sum('amount_paid');
        $bookings_count = $agent->transactions()->where('type', 'BOOKING')->count();
        $topup_values_sum = $agent->transactions()->where('type', 'TOPUP')->sum('value');
        $topup_paid_sum = $agent->transactions()->where('type', 'TOPUP')->sum('amount_paid');
        $topup_count = $agent->transactions()->where('type', 'TOPUP')->count();

        //commission
        $agent->commission = $topup_values_sum - $topup_paid_sum;


        $data = compact('agent', 'bookings_values_sum', 'bookings_paid_sum', 'bookings_count', 'topup_values_sum', 'topup_paid_sum', 'topup_count');

        Utility::$response['status'] = 200;
        Utility::$response['data'] = $data;
        Utility::$response['message'] = 'Success';
        return response()->json(Utility::$response);

    }


    public function postGetParks(Request $request)
    {


        //Get password of submitted email if it exists in DB
        // $agent = Agent::where('username', $request->username)->first();
        $parks = Park::with('state')->where('active',1)->where('boardable',1)->get();

        Utility::$response['status'] = 200;
        Utility::$response['data'] = $parks;
        Utility::$response['message'] = trans('auth.success');
        return response()->json(Utility::$response);

    }

    public function postGetDestinations(Request $request)
    {

        Utility::$response = Utility::checkParameters($request, array('park_id'));
        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);


        //Get password of submitted email if it exists in DB
        // $agent = Agent::where('username', $request->username)->first();
        $parks = Park::with('state')->where('active',1)->where('boardable',1)->where('id', '<>', $request->park_id)->get();
        $operators = Operator::where('active', 1)->get();

        Utility::$response['status'] = 200;
        Utility::$response['data'] = ['dests' => $parks, 'operators'=>$operators];
        Utility::$response['message'] = trans('auth.success');
        return response()->json(Utility::$response);

    }


    public function postGetTrips(Request $request)
    {

        Utility::$response = Utility::checkParameters($request, array('park_id', 'dest_id', 'date'));
        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);


        if(empty($request->operator_id)){
             $trips = Trip::with(['operator', 'sourcepark','destpark', 'busType'])->where('source_park_id', $request->park_id)->where('dest_park_id', $request->dest_id)->get();
        }else{
             $trips = Trip::with(['operator', 'sourcepark','destpark', 'busType'])->where('source_park_id', $request->park_id)->where('dest_park_id', $request->dest_id)->where('operator_id', $request->operator_id)->get();
         }

        Utility::$response['status'] = 200;
        Utility::$response['data'] = ['trips' => $trips];
        Utility::$response['message'] = trans('auth.success');
        return response()->json(Utility::$response);

    }

    public function postBookTrip(Request $request)
    {

        Utility::$response = Utility::checkParameters($request, array('trip_id', 'date', 'num_pass', 'contact_name', 'contact_phone', 'nok', 'nok_phone'));
        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);

        $trip = Trip::find($request->trip_id);

        $booking_code = $trip->operator->code.rand(100000,999999);

        $booking = Booking::firstOrCreate([
                    'trip_id' => $request->trip_id,
                    'date' => date('Y-m-d', strtotime($request->date)),
                    'passenger_count' => $request->num_pass,
                    'contact_name' => $request->contact_name,
                    'contact_phone' => $request->contact_phone,
                    'contact_email' => $request->contact_email,
                'next_of_kin' => $request->nok,
                'next_of_kin_phone' => $request->nok_phone,
                'status' => 'PAID',
                'paid_date' => date('Y-m-d H:i:s'),
                'final_cost' => $request->num_pass*$trip->fare,
                'unit_cost' => $trip->fare,
                'agent_id' => $request->agent_id,
                'booking_code'=>$booking_code
            ]);

        $k = 0;
        for ($i=1; $i < 10 ; $i++) {
            $p = 'psg'.$i;
            $pssg = $request->$p;
            // dd($pssg);
            if(!empty($pssg))
                $k=$i+1;
                //echo $i.''.$pssg.'<br>';
        }

        for ($i=1; $i < $k ; $i++) {
            $p = 'psg'.$i;
            $psg_name = $request->$p;
            // echo $booking->id.'<br>';
            $response =  Passenger::firstOrCreate([
                'booking_id'=>$booking->id,
                'name'=>$psg_name,
            ]);
        }

        $wallet_balance = 0;
        //deduct from agent wallet...
        if(isset($request->agent_id)){

            $at = new AgentTransaction;
            $at->type = 'BOOKING';
            $at->value = $booking->final_cost;
            $at->amount_paid = $booking->final_cost;
            $at->approved = true;
            $agent = Agent::find($request->agent_id);
            $agent->transactions()->save($at);
            $agent->balance = $agent->balance - $booking->final_cost;
            $agent->save();
            $wallet_balance = $agent->balance;

        }


        $booking = Booking::with('trip.sourcepark', 'trip.destpark')->find($booking->id);

        Utility::$response['status'] = 200;
        Utility::$response['data'] = ['booking'=>$booking, 'wallet_balance'=>$wallet_balance];
        Utility::$response['message'] = trans('auth.success');
        return response()->json(Utility::$response);

    }


    public function postTopup(Request $request)
    {
        Utility::$response = Utility::checkParameters($request, array('agent_id', 'amount'));
        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);


        //Get password of submitted email if it exists in DB
        $agent = Agent::find($request->agent_id);

        if(empty($agent)) {
           Utility::$response['status'] = 404;
           Utility::$response['message'] = trans('auth.no_account');

           return response()->json(Utility::$response);
        }


        $at  = new AgentTransaction;
        $at->value = ($agent->percentage * $request->amount / 100) + $request->amount;
        // $at->value = $request->amount;
        $at->amount_paid = $request->amount;
        $at->approved = true;
        $at->type = 'TOPUP';

        $agent->transactions()->save($at);
        $agent->balance = $agent->balance + $at->value;
        $agent->save();

        Utility::$response['status'] = 200;
        Utility::$response['data'] = $agent->balance;
        Utility::$response['message'] = 'Success';
        return response()->json(Utility::$response);

    }

    public function postTransactionHistory(Request $request)
    {
        Utility::$response = Utility::checkParameters($request, array('agent_id'));
        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);


        //Get password of submitted email if it exists in DB
        $agent = Agent::find($request->agent_id);

        if(empty($agent)) {
           Utility::$response['status'] = 404;
           Utility::$response['message'] = trans('auth.no_account');

           return response()->json(Utility::$response);
        }

        $transactions = null;
        if(isset($request->type) && !empty($request->type)){
            $transactions = $agent->transactions()->where('type', $request->type)->orderBy('created_at', 'desc')->get();
        }else{
            $transactions = $agent->transactions()->orderBy('created_at', 'desc')->get();
        }


        Utility::$response['status'] = 200;
        Utility::$response['data'] = array('transactions'=>$transactions);
        Utility::$response['message'] = 'Success';
        return response()->json(Utility::$response);

    }


    public function postCheckBooking(Request $request){

        Utility::$response = Utility::checkParameters($request, array('booking_code', 'contact_phone'));
        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);


        $booking = Booking::with('trip.sourcepark', 'trip.destpark')->where('booking_code', $request->booking_code)->first();
        if(empty($booking)) {
           Utility::$response['status'] = 404;
           Utility::$response['message'] = 'Booking not found!';

           return response()->json(Utility::$response);
        }


        if($booking->contact_phone != $request->contact_phone)   {
           Utility::$response['status'] = 404;
           Utility::$response['message'] = 'Booking not found!';

           return response()->json(Utility::$response);
        }


        Utility::$response['status'] = 200;
        Utility::$response['data'] = $booking;
        Utility::$response['message'] = 'Success';
        return response()->json(Utility::$response);


    }


    public function postBookingHistory(Request $request)
    {
        Utility::$response = Utility::checkParameters($request, array('agent_id'));
        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);


        //Get password of submitted email if it exists in DB
        $agent = Agent::find($request->agent_id);

        if(empty($agent)) {
           Utility::$response['status'] = 404;
           Utility::$response['message'] = trans('auth.no_account');

           return response()->json(Utility::$response);
        }

        $bookings = $agent->bookings()->with('trip.sourcepark', 'trip.destpark')->orderBy('created_at', 'desc')->get();



        Utility::$response['status'] = 200;
        Utility::$response['data'] = $bookings;
        Utility::$response['message'] = 'Success';
        return response()->json(Utility::$response);

    }


    public function getMigrate(){


        // $ops = DB::connection('mysql_fmr')->select('SELECT * FROM operators');

        // // dd($ops);

        // foreach($ops as $op){

        //     $o = new Operator;
        //     $o->id = $op->id;
        //     $o->name = $op->name;
        //     $o->address = $op->address;
        //     $o->contact_person = $op->contact_person;
        //     $o->phone = $op->phone;
        //     $o->details = $op->details;
        //     $o->website = $op->website;
        //     $o->active = $op->active;
        //     $o->commission = 5;
        //     $o->created_at = $op->created;
        //     $o->updated_at = $op->modified;
        //     $o->save();

        // }

        // $parks = DB::connection('mysql_fmr')->select('SELECT * FROM parks WHERE state_id < 40');

        // // dd($parks);

        // foreach($parks as $op){

        //     $o = new Park;
        //     $o->name = $op->name;
        //     $o->address = $op->address;
        //     $o->state_id = $op->state_id;
        //     $o->boardable = $op->boardable;
        //     $o->active = $op->active;
        //     $o->created_at = $op->created;
        //     $o->updated_at = $op->modified;
        //     $o->save();

        // }

        $trips = DB::connection('mysql_fmr')->select('SELECT * FROM trips WHERE source_park_id < 173 AND destination_park_id < 171');

        // dd($trips[0]->toArray());

        foreach($trips as $trip){


            $j = DB::connection('mysql_fmr')->select('SELECT * FROM operators WHERE id = '.$trip->operator_id);
            if(empty($j))
                continue;

            $j = $j[0];
            $op = Operator::where('name', $j->name)->first();


            $t = new Trip;
            $t->id = $trip->id;
            $t->name = $trip->name;
            $t->operator_id = $op->id;
            $t->source_park_id = $trip->source_park_id;
            $t->dest_park_id = $trip->destination_park_id;
            $t->departure_time = $trip->departure_time;
            $t->duration = $trip->duration;
            $t->fare = $trip->fare;
            $t->bus_type_id = rand(1,3);
            $t->no_of_seats = $trip->num_of_seats;
            $t->active = 1;
            $t->ac = rand(0,1);
            $t->tv = rand(0,1);
            $t->security = rand(0,1);
            $t->passport = rand(0,1);
            $t->insurance = rand(0,1);
            $t->created_at = $trip->created;
            $t->updated_at = $trip->modified;
            $t->save();

            $t->operator_trip_id = $t->id;
            $t->save();

        }

    }
}