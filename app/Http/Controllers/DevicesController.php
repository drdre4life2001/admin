<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Device;
use App\Models\Operator;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class DevicesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {   

        $op_id = $request->session()->get('operator_id');
        if(empty($op_id)){
            $request->session()->flash('select', 'Please select an Operator');
            return redirect()->action('OperatorsController@index');
        }
        $devices = Device::where('operator_id', $op_id)->paginate(100000);
        $page_title = 'devices';
        return view('devices.index', compact('devices', 'page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $page_title = 'Add device';

        $op_id = $request->session()->get('operator_id');
        if(empty($op_id)){
            $request->session()->flash('select', 'Please select an Operator');
            return redirect()->action('OperatorsController@index');
        }
        $operator_name = Operator::where('id', $op_id)->first()->name;

        return view('devices.create', compact('page_title', 'operator_name'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $op_id = $request->session()->get('operator_id');
        // dd($op_id);
            $device = Device::firstOrCreate([
                    'name' => $request->name,
                    'operator_id' => $op_id,
                    'serial_number' => $request->serial_number,
            ]);


        Session::flash('flash_message', 'Device added!');

        return redirect('devices');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $device = Device::findOrFail($id);

        $page_title = 'View device';
        return view('devices.show', compact('device', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $device = Device::findOrFail($id);

        $page_title = 'Edit device';
        return view('devices.edit', compact('device', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        
        $device = Device::findOrFail($id);
        $device->update($request->all());

        Session::flash('flash_message', 'Device updated!');

        return redirect('devices');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Device::destroy($id);

        Session::flash('flash_message', 'Device deleted!');

        return redirect('devices');
    }

}
