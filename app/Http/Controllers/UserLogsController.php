<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\UserLog;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Auth;

class UserLogsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $userlogs = UserLog::paginate(100000);

        $page_title = 'userlogs';

        return view('userlogs.index', compact('userlogs', 'page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Add userlog';

        return view('userlogs.create', compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['user_id' => 'required', 'log_type' => 'required', ]);

        UserLog::create($request->all());

        Session::flash('flash_message', 'UserLog added!');

        return redirect('userlogs');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $userlog = UserLog::findOrFail($id);

        $page_title = 'View userlog';
        return view('userlogs.show', compact('userlog', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $userlog = UserLog::findOrFail($id);

        $page_title = 'Edit userlog';
        return view('userlogs.edit', compact('userlog', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['user_id' => 'required', 'log_type' => 'required', ]);

        $userlog = UserLog::findOrFail($id);
        $userlog->update($request->all());

        Session::flash('flash_message', 'UserLog updated!');

        return redirect('userlogs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        UserLog::destroy($id);

        Session::flash('flash_message', 'UserLog deleted!');

        return redirect('userlogs');
    }

}
