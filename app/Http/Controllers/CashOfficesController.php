<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\CashOffice;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class CashOfficesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $cashoffices = CashOffice::paginate(100000);

        $page_title = 'cashoffices';

        return view('cashoffices.index', compact('cashoffices', 'page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Add cashoffice';

        return view('cashoffices.create', compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        CashOffice::create($request->all());

        Session::flash('flash_message', 'CashOffice added!');

        return redirect('cashoffices');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $cashoffice = CashOffice::findOrFail($id);

        $page_title = 'View cashoffice';
        return view('cashoffices.show', compact('cashoffice', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $cashoffice = CashOffice::findOrFail($id);

        $page_title = 'Edit cashoffice';
        return view('cashoffices.edit', compact('cashoffice', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        $cashoffice = CashOffice::findOrFail($id);
        $cashoffice->update($request->all());

        Session::flash('flash_message', 'CashOffice updated!');

        return redirect('cashoffices');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        CashOffice::destroy($id);

        Session::flash('flash_message', 'CashOffice deleted!');

        return redirect('cashoffices');
    }

}
