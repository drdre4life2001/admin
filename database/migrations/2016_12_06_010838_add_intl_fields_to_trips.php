<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIntlFieldsToTrips extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trips', function (Blueprint $table) {
            $table->boolean('is_intl_trip')->default(FALSE)->after('active');
            $table->double('virgin_passport_fare')->default(0.00)->after('is_intl_trip');
            $table->double('no_passport_fare')->default(0.00)->after('virgin_passport_fare');
            $table->double('round_trip_virgin_passport_fare')->default(0.00)->after('no_passport_fare');
            $table->double('round_trip_no_passport_fare')->default(0.00)->after('round_trip_virgin_passport_fare');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trips', function (Blueprint $table) {
            $table->dropColumn('round_trip_no_passport_fare');
            $table->dropColumn('round_trip_virgin_passport_fare');
            $table->dropColumn('no_passport_fare');
            $table->dropColumn('virgin_passport_fare');
            $table->dropColumn('is_intl_trip');

        });
    }
}
