<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTermsAndBookingRulesToOperators extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('operators', function (Blueprint $table) {
            
            $table->text('booking_rules')->nullable()->after('commission');
            $table->text('local_terms')->nullable()->after('booking_rules');
            $table->text('intl_terms')->nullable()->after('local_terms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('operators', function (Blueprint $table) {
            
            $table->dropColumn('booking_rules');
            $table->dropColumn('local_terms');
            $table->dropColumn('intl_terms');
        });
    }
}
