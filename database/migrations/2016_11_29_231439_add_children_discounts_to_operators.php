<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChildrenDiscountsToOperators extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('operators', function (Blueprint $table) {
            $table->double('local_children_discount')->default(0)->after('booking_rules');
            $table->double('intl_children_discount')->default(0)->after('local_children_discount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('operators', function (Blueprint $table) {
            
            $table->dropColumn('local_children_discount');
            $table->dropColumn('intl_children_discount');
        });
    }
}
