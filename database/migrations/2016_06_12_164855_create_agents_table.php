<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAgentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('agents', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('email');
			$table->string('username');
			$table->string('password', 60);
			$table->dateTime('last_login')->nullable();
			$table->string('phone');
			$table->boolean('active')->default(1);
			$table->text('address', 65535)->nullable();
			$table->text('notes', 65535)->nullable();
			$table->float('balance', 10, 0)->default(0);
			$table->float('percentage')->default(0.00);
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('agents');
	}

}
