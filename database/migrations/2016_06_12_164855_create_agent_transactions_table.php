<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAgentTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('agent_transactions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('agent_id')->unsigned()->index('agent_purchases_agent_id_index');
			$table->string('type', 60);
			$table->float('value', 10, 0);
			$table->float('amount_paid', 10, 0);
			$table->boolean('approved');
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('agent_transactions');
	}

}
