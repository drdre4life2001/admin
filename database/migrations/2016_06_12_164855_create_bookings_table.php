<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBookingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bookings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('trip_id')->unsigned()->index();
			$table->date('date');
			$table->integer('passenger_count');
			$table->float('unit_cost', 10, 0);
			$table->float('final_cost', 10, 0);
			$table->integer('payment_method_id')->unsigned()->nullable()->index();
			$table->integer('bank_id')->unsigned()->nullable()->index();
			$table->string('status')->default('PENDING');
			$table->dateTime('paid_date')->nullable();
			$table->string('booking_code')->nullable()->default('');
			$table->string('contact_name');
			$table->string('contact_phone');
			$table->string('contact_email', 50)->nullable();
			$table->string('next_of_kin')->nullable()->default('');
			$table->string('next_of_kin_phone')->default('');
			$table->integer('parent_booking_id')->unsigned()->nullable()->index();
			$table->integer('agent_id')->unsigned()->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bookings');
	}

}
